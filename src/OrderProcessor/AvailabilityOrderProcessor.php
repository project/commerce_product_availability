<?php

namespace Drupal\commerce_product_availability\OrderProcessor;

use Drupal\commerce\Context;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\OrderProcessorInterface;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\commerce_product_availability\AvailabilityChecker\ProductAvailabilityAvailabilityChecker;

/**
 * The commerce_product_availability order processor.
 *
 * We are using the order processor to remove unavailable products from the
 * order, so unavailable products can not be purchased.
 */
class AvailabilityOrderProcessor implements OrderProcessorInterface {

  /**
   * Constructs a new AvailabilityOrderProcessor object.
   */
  public function __construct(
    protected readonly ProductAvailabilityAvailabilityChecker $availabilityChecker,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function process(OrderInterface $order) {
    foreach ($order->getItems() as $orderItem) {
      $variation = $orderItem->getPurchasedEntity();
      // If the order item has no purchased entity, or is not a variation,
      // skip it:
      if ($variation === NULL || !$variation instanceof ProductVariationInterface) {
        continue;
      }

      $context = new Context($order->getCustomer(), $order->getStore());
      // If the variation is unavailable, remove it from the order:
      if ($this->availabilityChecker->check($orderItem, $context)->isUnavailable()) {
        $order->removeItem($orderItem);
        $orderItem->delete();
      }
    }
  }

}
