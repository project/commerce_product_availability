<?php

namespace Drupal\commerce_product_availability\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure settings for the Commerce Product Availability module.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['commerce_product_availability.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_product_availability_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config('commerce_product_availability.settings');

    $form['availability'] = [
      '#type' => 'details',
      '#title' => $this->t('Availability'),
      '#description' => $this->t('Settings related to the global availability settings, for commerce products using the Product Availability field.'),
      '#open' => TRUE,
    ];

    $form['availability']['min_delivery_period_fallback'] = [
      '#type' => 'number',
      '#title' => $this->t('Minimum delivery period global fallback'),
      '#description' => $this->t('Define the fallback for all "Product Availability" fields "Minimum Delivery Period" value, if they are left empty. Leave empty for no fallback.'),
      '#default_value' => $config->get('min_delivery_period_fallback') ?? NULL,
      '#min' => 0,
      '#field_suffix' => 'days',
    ];
    $form['availability']['max_delivery_period_fallback'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum delivery period global fallback'),
      '#description' => $this->t('Define the fallback for all "Product Availability" fields "Maximum Delivery Period" value, if they are left empty. Leave empty for no fallback.'),
      '#default_value' => $config->get('max_delivery_period_fallback') ?? NULL,
      '#min' => 0,
      '#field_suffix' => 'days',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('commerce_product_availability.settings')
      ->set('min_delivery_period_fallback', $form_state->getValue('min_delivery_period_fallback'))
      ->set('max_delivery_period_fallback', $form_state->getValue('max_delivery_period_fallback'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
