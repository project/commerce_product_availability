<?php

declare(strict_types=1);

namespace Drupal\commerce_product_availability\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Defines the Product Availability simple widget.
 *
 * @FieldWidget(
 *   id = "commerce_product_availability_product_availability_simple",
 *   label = @Translation("Product Availability Simple"),
 *   field_types = {"commerce_product_availability_product_availability"},
 * )
 */
class ProductAvailabilityWidgetSimple extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    /** @var \Drupal\commerce_product_availability\Plugin\Field\FieldType\ProductAvailabilityItem $item */
    $item = $items[$delta];
    $element['availability'] = [
      '#type' => 'details',
      '#title' => $this->t('Availability'),
      '#description' => $this->t('The availability settings of the product.<br>These values are taken from the <a href=":url" target="_blank">Google Merchant documentation</a>.', [
        ':url' => Url::fromUri('https://support.google.com/merchants/answer/6324448')->toString(),
      ]),
      '#open' => TRUE,
    ];
    $element['availability']['orderable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Orderable'),
      '#description' => $this->t('Whether the product is orderable. Note, that this setting alone defines, whether a product is purchasable or not.'),
      '#default_value' => $item->orderable ?? TRUE,
    ];
    $element['availability']['availability_status'] = [
      '#type' => 'select',
      '#title' => $this->t('Availability Status'),
      '#description' => $this->t('The availability status of the product. Note, that this setting does NOT effect the purchasability of the product.'),
      '#default_value' => $item->availability_status ?? 'in_stock',
      '#options' => commerce_product_availability_get_availability_status_options(),
    ];
    return $element;
  }

}
