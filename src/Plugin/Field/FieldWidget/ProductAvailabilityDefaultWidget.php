<?php

declare(strict_types=1);

namespace Drupal\commerce_product_availability\Plugin\Field\FieldWidget;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the Product Availability default widget.
 *
 * @FieldWidget(
 *   id = "commerce_product_availability_product_availability_default",
 *   label = @Translation("Product Availability Default"),
 *   field_types = {"commerce_product_availability_product_availability"},
 * )
 */
class ProductAvailabilityDefaultWidget extends WidgetBase {

  /**
   * The configuration object.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $globalSettings;

  /**
   * Constructs a ProductAvailabilityDefaultWidget object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, ConfigFactoryInterface $config_factory) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->globalSettings = $config_factory->get('commerce_product_availability.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($plugin_id, $plugin_definition, $configuration['field_definition'], $configuration['settings'], $configuration['third_party_settings'], $container->get('config.factory'));
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    /** @var \Drupal\commerce_product_availability\Plugin\Field\FieldType\ProductAvailabilityItem $item */
    $item = $items[$delta];
    $element['availability'] = [
      '#type' => 'details',
      '#title' => $this->t('Availability'),
      '#description' => $this->t('The availability settings of the product.<br>These values are taken from the <a href=":url" target="_blank">Google Merchant documentation</a>.', [
        ':url' => Url::fromUri('https://support.google.com/merchants/answer/6324448')->toString(),
      ]),
      '#open' => TRUE,
    ];
    $element['availability']['orderable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Orderable'),
      '#description' => $this->t('Whether the product is orderable. Note, that this setting alone defines, whether a product is purchasable or not.'),
      '#default_value' => $item->orderable ?? TRUE,
    ];
    $element['availability']['availability_status'] = [
      '#type' => 'select',
      '#title' => $this->t('Availability Status'),
      '#description' => $this->t('The availability status of the product. Note, that this setting does NOT effect the purchasability of the product.'),
      '#default_value' => $item->availability_status ?? 'in_stock',
      '#options' => commerce_product_availability_get_availability_status_options(),
    ];
    $element['availability']['availability_date_value'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Available from'),
      '#description' => $this->t('The date and time when the product will be available.'),
      '#date_increment' => 1,
      '#default_value' => !empty($item->availability_date_object) ? $this->createDateDefaultValue($item->availability_date_object) : NULL,
      '#date_timezone' => date_default_timezone_get(),
    ];
    if ($this->fieldDefinition->getSetting('clear_availability_date_in_stock')) {
      $element['availability']['availability_date_value']['#description'] .= $this->t('<br><strong>Note</strong>, that "Clear availability date when in stock" is enabled! If the product is in stock, the date will be cleared after save!');
    }
    // If clear_availability_date_in_stock is enabled and the product is in
    // stock, we hide the date field:
    // @todo Comment this in again, once
    // https://www.drupal.org/project/drupal/issues/2419131 is fixed:
    // if ((bool) $this->fieldDefinition->getSetting
    // ('clear_availability_date_in_stock')) {
    // $element['availability']['availability_date_value']['#states']
    // ['invisible'][':input[name="field_availability[' . $delta . ']
    // [availability][availability_status]"]'] = ['value' => 'in_stock'];
    // }
    $element['availability']['min_delivery_period'] = [
      '#type' => 'number',
      '#title' => $this->t('Minimum Delivery Period'),
      '#description' => $this->t('The minimum time it takes to deliver the product.'),
      '#default_value' => $item->min_delivery_period ?? NULL,
      '#min' => 0,
      '#field_suffix' => 'days',
    ];
    $element['availability']['max_delivery_period'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum Delivery Period'),
      '#description' => $this->t('The maximum time it takes to deliver the product.'),
      '#default_value' => $item->max_delivery_period ?? NULL,
      '#min' => 0,
      '#field_suffix' => 'days',
    ];

    // If the fallbacks are set, use them as placeholder:
    if ($minFallback = $this->globalSettings->get('min_delivery_period_fallback')) {
      $element['availability']['min_delivery_period']['#placeholder'] = $minFallback;
    }
    if ($maxFallback = $this->globalSettings->get('max_delivery_period_fallback')) {
      $element['availability']['max_delivery_period']['#placeholder'] = $maxFallback;
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as &$item) {
      // Remove the details wrapper, so the values are properly saved:
      $item = $item['availability'];

      // If the delivery periods are empty, we need to set them NULL explicitly
      // as an empty string is not a valid value for an integer field:
      if (empty($item['min_delivery_period'])) {
        $item['min_delivery_period'] = NULL;
      }
      if (empty($item['max_delivery_period'])) {
        $item['max_delivery_period'] = NULL;
      }

      /** @var \Drupal\Core\Datetime\DrupalDateTime $dateTime */
      $dateTime = $item['availability_date_value'];

      // If the date is an array, the user did not provide the time. We need to
      // convert it to a DrupalDateTime object:
      // @todo This will make the validation fail, if the user did not provide a
      // time:
      if (is_array($dateTime)) {
        $dateTime = DrupalDateTime::createFromTimestamp(strtotime($dateTime['date']));
      }

      // If the date is empty or the product is in stock and the
      // "clear_availability_date_in_stock" setting is enabled, we don't need
      // the date. Meaning we set it NULL and continue:
      if (empty($dateTime) || ($item['availability_status'] == 'in_stock' && !empty($this->fieldDefinition->getSetting('clear_availability_date_in_stock')))) {
        $item['availability_date_value'] = NULL;
        continue;
      }

      // The widget form element type has transformed the value to a
      // DrupalDateTime object at this point. We need to convert it back to the
      // storage timezone and format.
      $item['availability_date_value'] = $dateTime->setTimezone(new \DateTimezone(DateTimeItemInterface::STORAGE_TIMEZONE))->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
    }
    return $values;
  }

  /**
   * Creates a default value for the date field.
   *
   * @param \DateTime $date
   *   The date object to create the default value from.
   *
   * @return mixed
   *   The default value for the date field.
   */
  protected function createDateDefaultValue($date) {
    // The date was created and verified during field_load(), so it is safe to
    // use without further inspection.
    $date->setTimezone(new \DateTimeZone(date_default_timezone_get()));
    return $date;
  }

}
