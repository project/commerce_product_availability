<?php

declare(strict_types=1);

namespace Drupal\commerce_product_availability\Plugin\Field\FieldFormatter;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime\Plugin\Field\FieldFormatter\DateTimeDefaultFormatter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'Product Availability' default formatter.
 *
 * Note, that we are extending the DateTimeDefaultFormatter class, because date
 * time formatting is quite complex and this saves us a lot of work.
 *
 * @FieldFormatter(
 *   id = "commerce_product_availability_product_availability_default",
 *   label = @Translation("Product Availability Default"),
 *   field_types = {"commerce_product_availability_product_availability"},
 * )
 */
class ProductAvailabilityDefaultFormatter extends DateTimeDefaultFormatter {

  const AVAILABLE = 'available';
  const LIMITED_AVAILABILITY = 'limited-availability';
  const UNAVAILABLE = 'unavailable';

  /**
   * The configuration object.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $globalSettings;

  /**
   * Constructs a new DateTimeDefaultFormatter.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Third party settings.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Entity\EntityStorageInterface $date_format_storage
   *   The date format entity storage.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, DateFormatterInterface $date_formatter, EntityStorageInterface $date_format_storage, ConfigFactoryInterface $config_factory) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings, $date_formatter, $date_format_storage);

    $this->globalSettings = $config_factory->get('commerce_product_availability.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('date.formatter'),
      $container->get('entity_type.manager')->getStorage('date_format'),
      $container->get('config.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
      'show_availability_status' => TRUE,
      'show_availability_date' => TRUE,
      'hide_availability_date_in_past' => TRUE,
      'show_min_delivery_period' => TRUE,
      'show_max_delivery_period' => TRUE,
      'show_availability_indicator' => TRUE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form = parent::settingsForm($form, $form_state);

    $form['show_availability_status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show availability status'),
      '#description' => $this->t('Show the availability status of the product.'),
      '#default_value' => $this->getSetting('show_availability_status'),
    ];
    $form['show_availability_date'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show availability date'),
      '#description' => $this->t('Show the date when the product is available.'),
      '#default_value' => $this->getSetting('show_availability_date'),
    ];
    $form['hide_availability_date_in_past'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide availability date if in past'),
      '#description' => $this->t('Hides the availability date if the date is in the past.'),
      '#default_value' => $this->getSetting('hide_availability_date_in_past'),
    ];
    $form['show_min_delivery_period'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show minimum delivery period'),
      '#description' => $this->t('Show the minimum delivery period of the product.'),
      '#default_value' => $this->getSetting('show_min_delivery_period'),
    ];
    $form['show_max_delivery_period'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show maximum delivery period'),
      '#description' => $this->t('Show the maximum delivery period of the product.'),
      '#default_value' => $this->getSetting('show_max_delivery_period'),
    ];
    $form['show_availability_indicator'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show availability indicator symbol'),
      '#description' => $this->t('Shows a colored availability indicator symbol, which is colored based on the availability of the product:<br>
        <ul>
          <li><strong>Green</strong>: The product is in stock & orderable.</li>
          <li><strong>Yellow</strong>: The product is on preorder / backorder / out of stock & orderable.</li>
          <li><strong>Red</strong>: The product is unorderable</li>
        </ul>. By default a UTF-8 icon truck symbol is used, this can be changed in the twig template.'),
      '#default_value' => $this->getSetting('show_availability_indicator'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    return parent::settingsSummary() + [
      $this->t('Show availability status: @show_availability_status', ['@show_availability_status' => (bool) $this->getSetting('show_availability_status') ? $this->t('Yes') : $this->t('No')]),
      $this->t('Show availability date: @show_availability_date', ['@show_availability_date' => (bool) $this->getSetting('show_availability_date') ? $this->t('Yes') : $this->t('No')]),
      $this->t('Hide availability date if in past: @hide_availability_date_in_past', ['@hide_availability_date_in_past' => (bool) $this->getSetting('hide_availability_date_in_past') ? $this->t('Yes') : $this->t('No')]),
      $this->t('Show minimum delivery period: @show_min_delivery_period', ['@show_min_delivery_period' => (bool) $this->getSetting('show_min_delivery_period') ? $this->t('Yes') : $this->t('No')]),
      $this->t('Show maximum delivery period: @show_max_delivery_period', ['@show_max_delivery_period' => (bool) $this->getSetting('show_max_delivery_period') ? $this->t('Yes') : $this->t('No')]),
      $this->t('Show availability indicator symbol: @show_availability_indicator', ['@show_availability_indicator' => (bool) $this->getSetting('show_availability_indicator') ? $this->t('Yes') : $this->t('No')]),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];
    foreach ($items as $delta => $item) {
      $elements[$delta] = $this->buildAvailabilityOutput(
          $langcode,
          !empty($item->orderable),
          $item->availability_status,
          $item->availability_date_object,
          (int) $item->min_delivery_period,
          (int) $item->max_delivery_period,
        );
    }
    return $elements;
  }

  /**
   * Builds the rendered availability output.
   *
   * @param string $langcode
   *   The language code.
   * @param bool $orderable
   *   Whether the product is orderable.
   * @param string|null $availabilityStatus
   *   The availability status of the product.
   * @param \Drupal\Core\Datetime\DrupalDateTime|null $date
   *   The date of availability.
   * @param int|null $minDeliveryPeriod
   *   The minimum delivery period in days.
   * @param int|null $maxDeliveryPeriod
   *   The maximum delivery period in days.
   *
   * @return array
   *   The rendered availability output.
   */
  protected function buildAvailabilityOutput($langcode, bool $orderable, ?string $availabilityStatus, ?DrupalDateTime $date, ?int $minDeliveryPeriod, ?int $maxDeliveryPeriod): array {
    // Hide the min availability status if the setting is disabled:
    $availabilityStatusLabel = NULL;
    if ($this->getSetting('show_availability_status')) {
      $availabilityStatusLabel = commerce_product_availability_get_availability_status_options()[$availabilityStatus];
    }
    // Hide the date if it is in the past and the setting is enabled OR
    // if the setting to show the date is disabled:
    // Note, I know, that it would be more performant if we negate this
    // statement and run "buildDateWithIsoAttribute" instead. But negating
    // this if statement, makes it unreadable:
    if ($date === NULL || ($this->getSetting('hide_availability_date_in_past') && $date->getTimestamp() < time()) || !$this->getSetting('show_availability_date')) {
      $renderedAvailabilityDate = NULL;
    }
    else {
      $renderedAvailabilityDate = $this->buildDateWithIsoAttribute($date);
    }

    // If the min / max delivery period is empty, we should use the global
    // fallback:
    if ($minDeliveryPeriod === NULL) {
      $minDeliveryPeriod = $this->globalSettings->get('min_delivery_period_fallback');
    }
    if ($maxDeliveryPeriod === NULL) {
      $maxDeliveryPeriod = $this->globalSettings->get('max_delivery_period_fallback');
    }
    // Hide the min delivery period if the setting is disabled:
    if (!$this->getSetting('show_min_delivery_period')) {
      $minDeliveryPeriod = NULL;
    }
    // Hide the max delivery period if the setting is disabled:
    if (!$this->getSetting('show_max_delivery_period')) {
      $maxDeliveryPeriod = NULL;
    }

    // Set the Availability truck color:
    $availabilityIndicator = NULL;
    if ($this->getSetting('show_availability_indicator')) {
      $availabilityIndicator = self::getAvailabilityIndicator($availabilityStatus, $orderable);
    }
    $build = [
      '#theme' => 'commerce_product_availability_product_availability_default',
      '#langcode' => $langcode,
      '#orderable' => $orderable,
      '#availability_status' => $availabilityStatus,
      '#availability_status_label' => $availabilityStatusLabel,
      '#availability_date' => $renderedAvailabilityDate,
      '#min_delivery_period' => $minDeliveryPeriod,
      '#max_delivery_period' => $maxDeliveryPeriod,
      '#availability_indicator' => $availabilityIndicator,
      '#attached' => [
        'library' => [
          'commerce_product_availability/availability_indicator',
        ],
      ],
    ];

    return $build;
  }

  /**
   * Get the availability indicator color.
   */
  public static function getAvailabilityIndicator(?string $availabilityStatus, bool $orderable): string {
    if ($orderable) {
      if ($availabilityStatus === 'in_stock') {
        return self::AVAILABLE;
      }
      else {
        return self::LIMITED_AVAILABILITY;
      }
    }
    return self::UNAVAILABLE;
  }

}
