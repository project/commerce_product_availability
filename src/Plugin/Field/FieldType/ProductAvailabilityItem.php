<?php

declare(strict_types=1);

namespace Drupal\commerce_product_availability\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the Product Availability field type.
 *
 * @FieldType(
 *   id = "commerce_product_availability_product_availability",
 *   label = @Translation("Product Availability"),
 *   description = @Translation("Stores product availability information"),
 *   category = "commerce",
 *   default_widget =
 *   "commerce_product_availability_product_availability_default",
 *   default_formatter =
 *   "commerce_product_availability_product_availability_default",
 * )
 */
class ProductAvailabilityItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings(): array {
    $settings = [
      'clear_availability_date_in_stock' => FALSE,
      'alter_add_to_cart_button' => 'no_altering',
    ];
    return $settings + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state): array {
    $element['alter_add_to_cart_button'] = [
      '#type' => 'radios',
      '#title' => $this->t('Alter add to cart button behavior'),
      '#description' => $this->t('Whether to alter the add to cart button, based on the "orderable" and "availability_status" values:<ul>
        <li><strong>No altering</strong>: The add to cart button will not be altered.</li>
        <li><strong>Disable if not orderable</strong>: The add to cart button will be disabled if the products "orderable" value is false.</li>
        <li><strong>Disable if not orderable and alter label accordingly</strong>: The add to cart button will be disabled and the "Add to cart" label will be modified as follows:
        <ul>
          <li>orderable && in_stock = "Add to cart"</li>
          <li>orderable && out_of_stock / preorder = "Preorder"</li>
          <li>orderable && backorder = "Backorder"</li>
          <li>!orderable && in_stock = "Unavailable"</li>
          <li>!orderable && out_of_stock / preorder / backorder = "Out of stock"</li>
        </ul></li>
        <li><strong>Remove</strong>: Remove the button, if the products "orderable" value is false.</li></ul>'),
      '#options' => [
        'no_altering' => $this->t('No altering'),
        'disable_only' => $this->t('Disable if not orderable'),
        'disable_and_alter_text' => $this->t('Disable if not orderable and alter label accordingly'),
        'remove' => $this->t('Remove'),
      ],
      '#default_value' => $this->getSetting('alter_add_to_cart_button'),
    ];
    $element['clear_availability_date_in_stock'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Clear availability date when in stock'),
      // @todo Add "<li>The availability date will not be shown on the field widget.</li>"
      // to the description, once
      // https://www.drupal.org/project/drupal/issues/2419131 is fixed, and the
      // field widget is able to hide the date:
      '#description' => $this->t('This setting will set the availability date to NULL if the "availability_status" is set to "in_stock". Note, that this will happen on the entity form "save" action.'),
      '#default_value' => $this->getSetting('clear_availability_date_in_stock'),
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty(): bool {
    return match ($this->get('availability_status')->getValue()) {
      NULL, '' => TRUE,
      default => FALSE,
    };
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {

    // @DCG
    // See /core/lib/Drupal/Core/TypedData/Plugin/DataType directory for
    // available data types.
    $properties['orderable'] = DataDefinition::create('boolean')
      ->setLabel(new TranslatableMarkup('Orderable'))
      ->setRequired(FALSE);
    $properties['availability_status'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Availability Status'))
      ->setRequired(TRUE);
    $properties['availability_date_value'] = DataDefinition::create('datetime_iso8601')
      ->setLabel(new TranslatableMarkup('Available on'))
      ->setRequired(FALSE);
    $properties['availability_date_object'] = DataDefinition::create('any')
      ->setLabel(new TranslatableMarkup('Computed date'))
      ->setDescription(new TranslatableMarkup('The computed DateTime object.'))
      ->setComputed(TRUE)
      ->setClass('\Drupal\datetime\DateTimeComputed')
      ->setSetting('date source', 'availability_date_value');
    $properties['min_delivery_period'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Minimum Delivery Period'))
      ->setRequired(FALSE);
    $properties['max_delivery_period'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Maximum Delivery Period'))
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array {

    $columns = [
      'availability_status' => [
        'type' => 'varchar',
        'description' => 'The availability status.',
        'length' => 64,
      ],
      'availability_date_value' => [
        'type' => 'varchar',
        'description' => 'The availability date.',
        'length' => 20,
      ],
      'orderable' => [
        'type' => 'int',
        'description' => 'Whether the product is orderable.',
        'size' => 'tiny',
        'not null' => TRUE,
      ],
      'min_delivery_period' => [
        'type' => 'int',
        'description' => 'The minimum delivery period.',
        'size' => 'normal',
      ],
      'max_delivery_period' => [
        'type' => 'int',
        'description' => 'The maximum delivery period.',
        'size' => 'normal',
      ],
    ];

    return [
      'columns' => $columns,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function onChange($property_name, $notify = TRUE) {
    // Enforce that the computed date is recalculated.
    if ($property_name == 'availability_date_value') {
      $this->availability_date_object = NULL;
    }
    parent::onChange($property_name, $notify);
  }

}
