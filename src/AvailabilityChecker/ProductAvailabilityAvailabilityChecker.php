<?php

namespace Drupal\commerce_product_availability\AvailabilityChecker;

use Drupal\commerce\Context;
use Drupal\commerce_order\AvailabilityCheckerInterface;
use Drupal\commerce_order\AvailabilityResult;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_product\Entity\ProductVariationInterface;

/**
 * The commerce_product_availability product availability checker.
 */
class ProductAvailabilityAvailabilityChecker implements AvailabilityCheckerInterface {

  /**
   * {@inheritdoc}
   */
  public function applies(OrderItemInterface $order_item) {
    return !empty($this->getAvailabilityFieldsByOrderItem($order_item));
  }

  /**
   * {@inheritdoc}
   */
  public function check(OrderItemInterface $order_item, Context $context) {
    $availabilityFields = $this->getAvailabilityFieldsByOrderItem($order_item);
    // If there are no availability fields, the current product doesn't apply
    // for our use case and we can return a neutral availability:
    if (empty($availabilityFields)) {
      return AvailabilityResult::neutral();
    }

    // We do not support multiple availability fields:
    $availabilityFieldName = reset($availabilityFields)->getName();
    $variation = $order_item->getPurchasedEntity();
    $fieldValues = $variation->get($availabilityFieldName);
    if (empty($fieldValues->orderable)) {
      return AvailabilityResult::unavailable();
    }
    return AvailabilityResult::neutral();
  }

  /**
   * Retrieves the availability fields for a given order item.
   *
   * @param \Drupal\commerce_order\Entity\OrderItemInterface $order_item
   *   The order item for which to retrieve the availability fields.
   *
   * @return array
   *   An array of availability fields.
   */
  public function getAvailabilityFieldsByOrderItem(OrderItemInterface $order_item) {
    $variation = $order_item->getPurchasedEntity();
    // If the order item has no purchased entity, or is not a variation return:
    if ($variation === NULL || !($variation instanceof ProductVariationInterface)) {
      return [];
    }
    $fieldDefinitions = $variation->getFieldDefinitions();
    return array_filter($fieldDefinitions, function ($fieldDefinition) {
      return $fieldDefinition->getType() === 'commerce_product_availability_product_availability';
    });
  }

}
