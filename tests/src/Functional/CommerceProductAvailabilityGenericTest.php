<?php

namespace Drupal\Tests\commerce_product_availability\Functional;

use Drupal\Tests\system\Functional\Module\GenericModuleTestBase;

/**
 * Generic module test for commerce_product_availability.
 *
 * @group commerce_product_availability
 */
class CommerceProductAvailabilityGenericTest extends GenericModuleTestBase {

  /**
   * {@inheritDoc}
   */
  protected function assertHookHelp(string $module): void {
    // Don't do anything here. Just overwrite this useless method, so we do
    // don't have to implement hook_help().
  }

}
