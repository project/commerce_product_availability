<?php

namespace Drupal\Tests\commerce_product_availability_webform_request\Functional;

use Drupal\Tests\system\Functional\Module\GenericModuleTestBase;

/**
 * Generic module test for commerce_product_availability_webform_request.
 *
 * @group commerce_product_availability_webform_request
 */
class CommerceProductAvailabilityWebformRequestGenericTest extends GenericModuleTestBase {

  /**
   * {@inheritDoc}
   */
  protected function assertHookHelp(string $module): void {
    // Don't do anything here. Just overwrite this useless method, so we do
    // don't have to implement hook_help().
  }

}
