<?php

/**
 * @file
 * Describe hooks provided by the Commerce Product Availability module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the availability status options.
 *
 * @param array $options
 *   The availability status options to alter.
 */
function hook_commerce_product_availability_availability_status_alter(&$options) {
  // Add an further availability option:
  $options['sold_out'] = t('Sold out');

  // Unset an unwanted availability option:
  unset($options['backorder']);
}
